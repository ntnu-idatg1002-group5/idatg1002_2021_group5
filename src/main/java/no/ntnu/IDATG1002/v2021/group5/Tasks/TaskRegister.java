package no.ntnu.IDATG1002.v2021.group5.Tasks;

import no.ntnu.IDATG1002.v2021.group5.GUI.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * The type Task register.
 */
public class TaskRegister {

    /**
     * The Tasks.
     */
    ObservableList<Task> tasks;

    /**
     * Instantiates a new Task register.
     */
    public TaskRegister()  {
        this.tasks = FXCollections.observableArrayList();
    }

    /**
     * Gets tasks.
     *
     * @return the tasks
     */
    public ObservableList<Task> getTasks()  {
        Main.readWrite.readFile();
        return tasks;
    }


    /**
     * Add task.
     *
     * @param task the task
     */
    public void addTask(Task task){
        // Guard condition
        if (task == null)  {
            throw new IllegalArgumentException("Task trying to be added is null");
        }
        else {
            tasks.add(task);
        }
    }

    /**
     * Remove task.
     *
     * @param task the task
     */
    public void removeTask(Task task){
        tasks.remove(task);
    }

    /**
     * Clear reg.
     */
    public void clearReg(){
        tasks.clear();
    }
}
