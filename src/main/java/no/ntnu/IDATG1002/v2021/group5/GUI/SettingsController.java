package no.ntnu.IDATG1002.v2021.group5.GUI;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Class to control what happens in the settings.fxml window
 */
public class SettingsController implements Initializable{

    @FXML private CheckBox priorityCheck = new CheckBox();
    @FXML private CheckBox statusCheck = new CheckBox();
    @FXML private CheckBox categoryCheck = new CheckBox();
    @FXML private CheckBox startDateCheck = new CheckBox();
    @FXML private CheckBox deadlineDateCheck = new CheckBox();
    @FXML private CheckBox deadlineTimeCheck = new CheckBox();
    @FXML private CheckBox taskDescriptionCheck = new CheckBox();
    @FXML private CheckBox finishedCheck = new CheckBox();
    @FXML private RadioButton defaultView = new RadioButton();
    @FXML private RadioButton darkMode = new RadioButton();
    @FXML private RadioButton pinkMode = new RadioButton();
    @FXML private RadioButton navyMode = new RadioButton();
    @FXML private RadioButton oceanMode = new RadioButton();

    private final static String STYLE_PATH = "/no/ntnu/IDATG1002/v2021/group5/GUI/";
    private final static String STYLE_DEFAULT = STYLE_PATH + "style_base.css";
    private final static String STYLE_DARK = STYLE_PATH + "style_dark.css";
    private final static String STYLE_PINK = STYLE_PATH + "style_pink.css";
    private final static String STYLE_NAVY = STYLE_PATH + "style_navy.css";
    private final static String STYLE_OCEAN = STYLE_PATH + "style_ocean.css";


    private final List<Boolean> cancel = HomeController.visibleColumns;
    private final ToggleGroup colorOption = new ToggleGroup();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        priorityCheck.setSelected(HomeController.visibleColumns.get(0));
        statusCheck.setSelected(HomeController.visibleColumns.get(1));
        categoryCheck.setSelected(HomeController.visibleColumns.get(2));
        startDateCheck.setSelected(HomeController.visibleColumns.get(3));
        deadlineDateCheck.setSelected(HomeController.visibleColumns.get(4));
        deadlineTimeCheck.setSelected(HomeController.visibleColumns.get(5));
        taskDescriptionCheck.setSelected(HomeController.visibleColumns.get(6));
        finishedCheck.setSelected(HomeController.visibleColumns.get(7));

        defaultView.setToggleGroup(colorOption);
        darkMode.setToggleGroup(colorOption);
        pinkMode.setToggleGroup(colorOption);
        navyMode.setToggleGroup(colorOption);
        oceanMode.setToggleGroup(colorOption);

        if(Main.getStylesheet().equals(STYLE_NAVY)) {
            navyMode.setSelected(true);
        }
        else if(Main.getStylesheet().equals(STYLE_DARK)) {
            darkMode.setSelected(true);
        }
        else if(Main.getStylesheet().equals(STYLE_PINK)) {
            pinkMode.setSelected(true);
        }
        else if(Main.getStylesheet().equals(STYLE_OCEAN)) {
            oceanMode.setSelected(true);
        }
        else {
            defaultView.setSelected(true);
        }
    }

    /**
     * Method to return back to Home screen
     * if cancel is pressed it will cancel all options selected
     * @param event event that gets hold of the running stage of the program
     */
    @FXML
    private void cancelButton(ActionEvent event)throws IOException{
        int i = 0;
        while (i < HomeController.visibleColumns.size()){
            HomeController.visibleColumns.set(i,cancel.get(i));
            i++;
        }
        returnHome(event);
    }

    /**
     * Return home.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    // Return home som laster stylesheet
    public void returnHome(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();

        if (Main.getPreviousWindow().equalsIgnoreCase("Archive"))  {
            loader.setLocation(getClass().getResource("Archive.fxml"));
        }
        else {
            loader.setLocation(getClass().getResource("Home.fxml"));
        }

        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene.getStylesheets().add(Main.getStylesheet());

        stage.setScene(scene);
    }

    /**
     * Method to save the changed made in settings.
     * @param event make the user go back to the home window
     */
    @FXML
    private void saveButton(ActionEvent event)throws IOException{
        Main.readWrite.writeSettings();

        returnHome(event);
    }

    /**
     * Method to check if the checkBoxes is marked or not.
     * will change the visibility of the column depending on if the chechBoc is marked or not. True for visible and
     * false for hidden.
     */
    @FXML
    private void visibleColumn(){
        if(priorityCheck.isSelected()){
            HomeController.visibleColumns.set(0,true);
        }
        else {
            HomeController.visibleColumns.set(0,false);
        }

        if (statusCheck.isSelected()) {
            HomeController.visibleColumns.set(1, true);
        }
        else{
            HomeController.visibleColumns.set(1,false);
        }

        if (categoryCheck.isSelected()){
            HomeController.visibleColumns.set(2,true);
        }
        else {
            HomeController.visibleColumns.set(2,false);
        }

        if (startDateCheck.isSelected()){
            HomeController.visibleColumns.set(3,true);
        }
        else {
            HomeController.visibleColumns.set(3,false);
        }

        if (deadlineDateCheck.isSelected()){
            HomeController.visibleColumns.set(4,true);
        }
        else {
            HomeController.visibleColumns.set(4,false);
        }

        if (deadlineTimeCheck.isSelected()){
            HomeController.visibleColumns.set(5,true);
        }
        else {
            HomeController.visibleColumns.set(5,false);
        }

        if (taskDescriptionCheck.isSelected()){
            HomeController.visibleColumns.set(6,true);
        }
        else {
            HomeController.visibleColumns.set(6,false);
        }

        if (finishedCheck.isSelected()){
            HomeController.visibleColumns.set(7,true);
        }
        else {
            HomeController.visibleColumns.set(7,false);
        }
    }

    /**
     * Function that controls which stylesheet that will be used based on which radioButton is selected
     */
    @FXML
    private void setDefaultView(){
        if (defaultView.isSelected()){
            Main.setStylesheet(STYLE_DEFAULT);
        } else if (darkMode.isSelected()){
            Main.setStylesheet(STYLE_DARK);
        } else if (pinkMode.isSelected()){
            Main.setStylesheet(STYLE_PINK);
        } else if (navyMode.isSelected()){
            Main.setStylesheet(STYLE_NAVY);
        }
        else if (oceanMode.isSelected()){
            Main.setStylesheet(STYLE_OCEAN);
        }
    }

    /**
     * Gets style default.
     *
     * @return the style default
     */
    public static String getStyleDefault() {
        return STYLE_DEFAULT;
    }

    /**
     * Gets style dark.
     *
     * @return the style dark
     */
    public static String getStyleDark() {
        return STYLE_DARK;
    }

    /**
     * Gets style pink.
     *
     * @return the style pink
     */
    public static String getStylePink() {
        return STYLE_PINK;
    }

    /**
     * Gets style navy.
     *
     * @return the style navy
     */
    public static String getStyleNavy() {
        return STYLE_NAVY;
    }
}
