package no.ntnu.IDATG1002.v2021.group5.GUI;

import no.ntnu.IDATG1002.v2021.group5.Tasks.ReadWrite;
import no.ntnu.IDATG1002.v2021.group5.Tasks.TaskRegister;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * The type Main.
 */
public class Main extends Application {

    /**
     * The constant taskRegister.
     */
    public static TaskRegister taskRegister = new TaskRegister();
    /**
     * The constant readWrite.
     */
    public static ReadWrite readWrite;
    private static String stylesheet = SettingsController.getStyleDefault();
    private static String previousWindow; // Used to return to previous window from edit task/settings

    static {
        try {
            ReadWrite.createFiles();
            readWrite = new ReadWrite();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Home.fxml"));
        primaryStage.setTitle("Planda");
        Scene main = new Scene(root);
        main.getStylesheets().add(stylesheet);
        primaryStage.setScene(main);
        primaryStage.show();
    }

    /**
     * Gets stylesheet.
     *
     * @return the stylesheet
     */
    public static String getStylesheet() {
        return stylesheet;
    }

    /**
     * Sets stylesheet.
     *
     * @param stylesheet the stylesheet
     */
    public static void setStylesheet(String stylesheet) {
        Main.stylesheet = stylesheet;
    }

    /**
     * Gets previous window.
     *
     * @return the previous window
     */
    public static String getPreviousWindow() {
        return previousWindow;
    }

    /**
     * Sets previous window.
     *
     * @param previousWindow the previous window
     */
    public static void setPreviousWindow(String previousWindow) {
        Main.previousWindow = previousWindow;
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
