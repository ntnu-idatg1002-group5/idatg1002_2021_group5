package no.ntnu.IDATG1002.v2021.group5.GUI;

import no.ntnu.IDATG1002.v2021.group5.Tasks.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * The type Add controller.
 */
public class AddController implements Initializable {
    /**
     * The Add new task button.
     */
    public Button addNewTaskButton;
    /**
     * The Cancel button.
     */
    public Button cancelButton;

    @FXML private TextField taskNameField;
    @FXML private ChoiceBox priorityBox;
    @FXML private ChoiceBox statusBox;
    @FXML private ChoiceBox catBox;
    @FXML private DatePicker startDateField;
    @FXML private DatePicker endDateField;
    @FXML private TextField deadlineField;
    @FXML private TextArea taskDescriptionField;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        priorityBox.getItems().addAll(Task.getPriorityNames());
        priorityBox.setValue(Task.getPriorityNames().get(1));
        statusBox.getItems().addAll("Not Started", "Ongoing");
        statusBox.setValue("Not Started");
        catBox.getItems().addAll("Work", "School", "Sport", "Hobby", "Home", "Other");
        catBox.setValue("Work");
        startDateField.setValue(LocalDate.now());
        endDateField.setValue(LocalDate.now());
    }

    /**
     * Return home.
     * By loading an FXML-loader and adding the fxml to a scene.
     * After that the scene is added to the stage.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void returnHome(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Home.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene.getStylesheets().add(Main.getStylesheet());

        stage.setScene(scene);
    }

    /**
     * Add new task clicked.
     * If the user decides to not input anything, placeholder information
     * will be placed. Once the user presses the button to add the new task,
     * The task is written to the JSON file, and also added to the register-class.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void addNewTaskClicked(ActionEvent event) throws IOException {
        if(taskNameField.getText().isEmpty()){
            taskNameField.setText("A new task");
        }
        if(deadlineField.getText().isEmpty()){
            deadlineField.setText("12:00");
        }
        if(taskDescriptionField.getText().isEmpty()){
            taskDescriptionField.setText("This task needs a description.");
        }
        Task newTask = new Task(taskNameField.getText(), priorityBox.getValue().toString(), statusBox.getValue().toString(), catBox.getValue().toString(), startDateField.getValue(), endDateField.getValue(), deadlineField.getText(),taskDescriptionField.getText(), "false");
        Main.readWrite.writeToFile(newTask);
        Main.readWrite.readFile();

        returnHome(event);
    }

    /**
     * Cancel button.
     * If the cancle button is pressed, the user is taken back to the
     * "home.fxml".
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void cancelButton(ActionEvent event) throws IOException {
        returnHome(event);
    }

}
