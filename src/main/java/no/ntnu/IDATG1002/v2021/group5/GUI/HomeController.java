package no.ntnu.IDATG1002.v2021.group5.GUI;

import javafx.util.Duration;
import no.ntnu.IDATG1002.v2021.group5.Tasks.Task;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.sql.Time;
import java.time.LocalDate;
import java.util.*;

/**
 * The type Home controller.
 */
public class HomeController implements Initializable {

    @FXML private TableView<Task> tableView;
    @FXML private TableColumn<Task, String> taskNameColumn;
    @FXML private TableColumn<Task, String> priorityColumn;
    @FXML private TableColumn<Task, String> statusColumn;
    @FXML private TableColumn<Task, String> categoryColumn;
    @FXML private TableColumn<Task, LocalDate> startDateColumn;
    @FXML private TableColumn<Task, LocalDate> deadlineDateColumn;
    @FXML private TableColumn<Task, Time> deadlineClockColumn;
    @FXML private TableColumn<Task, String> taskDescriptionColumn;
    @FXML private TableColumn<Task, Boolean> finishedBoolColumn;

    @FXML private Button addButton;
    @FXML private Button editButton;
    @FXML private Button deleteButton;
    @FXML private Button archiveButton;
    @FXML private Button settingsButton;

    /**
     * The Data.
     */
    public ObservableList<Task> data;
    /**
     * The Unfinished tasks.
     */
    public ObservableList<Task> unfinishedTasks;
    /**
     * The Visible columns.
     */
    public static List<Boolean> visibleColumns;

    /**
     * The constant answer.
     */
// Used for delete confirmation
    static boolean answer;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        visibleColumns = new ArrayList<>();
        tableView.setEditable(true);

        taskNameColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("taskName"));
        priorityColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("priority"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("status"));
        categoryColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("category"));
        startDateColumn.setCellValueFactory(new PropertyValueFactory<Task, LocalDate>("startDate"));
        deadlineDateColumn.setCellValueFactory(new PropertyValueFactory<Task, LocalDate>("endDate"));
        deadlineClockColumn.setCellValueFactory(new PropertyValueFactory<Task, Time>("deadlineClock"));
        taskDescriptionColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("taskDescription"));
        finishedBoolColumn.setCellValueFactory(param -> param.getValue().obsBoolean());
        /*
        * code is from:: https://stackoverflow.com/questions/39270033/javafx-checkboxtablecell-catch-clickevent
         */
        finishedBoolColumn.setCellFactory(p -> {
            CheckBox checkBox = new CheckBox();
            TableCell<Task, Boolean> tableCell = new TableCell<>(){
                @Override
                protected void updateItem(Boolean item, boolean empty) {

                    super.updateItem(item, empty);
                    if (empty || item == null)
                        setGraphic(null);
                    else {
                        setGraphic(checkBox);
                        checkBox.setSelected(item);
                    }
                }
            };

            checkBox.addEventFilter(MouseEvent.MOUSE_PRESSED, event ->
            {
                try {
                    changeFinishedState(checkBox, (Task) tableCell.getTableRow().getItem());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            checkBox.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
                if(event.getCode() == KeyCode.SPACE) {
                    try {
                        changeFinishedState(checkBox, (Task) tableCell.getTableRow().getItem());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            tableCell.setAlignment(Pos.CENTER);
            tableCell.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

            return tableCell;
        });


        // Comparing priorities by their index rather than alphabetically
        priorityColumn.setComparator(Comparator.comparingInt(p -> Task.getPriorityNames().indexOf(p)));

        //function checks which columns are going to be visible from the start of the program
        try {
            visible();
        } catch (IOException e) {
            e.printStackTrace();
        }

        data = Main.taskRegister.getTasks();


        data.removeIf(Task::isFinished);

        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.getItems().clear();
        tableView.setItems(data);

        Main.setPreviousWindow("Home");

        setTooltipsForButtons();
    }

    /**
     * Function to move a task from not finished to finished
     * @param checkBox selects the checkBox to the row
     * @param item the task the checkBox is a part of
     * @throws IOException - throws IOException in case the program can't write to the task.json file
     */
    private void changeFinishedState(CheckBox checkBox, Task item) throws IOException {
        checkBox.setSelected(!checkBox.isSelected());
        Task tempTask = item;
        data.remove(item);
        Main.readWrite.removeTaskFromFile(item);
        tempTask.setFinished(new SimpleBooleanProperty(true));
        Main.readWrite.writeToFile(tempTask);
    }

    /**
     * Function to set a specific tooltip for a button tha will show with text what a button do
     */
    private void setTooltipsForButtons() {
        // set hover text for buttons
        addButton.setTooltip(newTooltip("Add task"));
        editButton.setTooltip(newTooltip("Edit task"));
        deleteButton.setTooltip(newTooltip("Delete task"));
        archiveButton.setTooltip(newTooltip("Go to archive"));
        settingsButton.setTooltip(newTooltip("Settings"));
    }

    /**
     * Function to make a Tooltip for a button, with delays configured
     * @param text parameter of the text that show on the tooltip
     * @return returns the created tooltip
     */
    private Tooltip newTooltip(String text) {
        Tooltip tooltip = new Tooltip(text);
        tooltip.setShowDelay(Duration.seconds(0));
        tooltip.setHideDelay(Duration.seconds(0));
        return tooltip;
    }

    /**
     * Add task button clicked. moves the main window over to the add task window.
     * It reloads the main scene with a new javafxml
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void addTaskButtonClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Add.fxml"));
        Scene addScene = new Scene(root);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        addScene.getStylesheets().add(Main.getStylesheet());
        window.setScene(addScene);
    }

    /**
     * Delete task button clicked. if a task is selected and this function is run it will delete a task from
     * the observableArrayList and the file the task is saved on
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void deleteTaskButtonClicked(ActionEvent event) throws IOException {
        if (!tableView.getSelectionModel().isEmpty())  {
            if(showDeleteConfirmationDialog()) {
                Task tempTask = tableView.getSelectionModel().getSelectedItem();
                data.remove(tempTask);
                Main.readWrite.removeTaskFromFile(tempTask);
            }
        }
    }

    /**
     * Displays a delete confirmation dialog. If the user confirms the delete,
     * <code>true</code> is returned.
     * <p>
     * KODE FRA ARNE STYVE: JavaFX-TableView-with-Dialog
     *
     * @return <code>true</code> if the user confirms the delete
     */
    public boolean showDeleteConfirmationDialog() {
        boolean deleteConfirmed = false;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete confirmation");
        alert.setHeaderText("Are you sure you want to delete this item?");

        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(Main.getStylesheet());

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent()) {
            deleteConfirmed = (result.get() == ButtonType.OK);
        }
        return deleteConfirmed;
    }

    /**
     * Edit task button clicked. When the button is pushed and a task is selected it will change scene from home to
     * edit scene, with information about the selected task
     *
     * @param event the event
     */
    public void editTaskButtonClicked(ActionEvent event){
        if (!tableView.getSelectionModel().isEmpty())
        {
            try  {
                Task task = tableView.getSelectionModel().getSelectedItem();
                changeScenes(event, task);
            }
            catch (IOException e)  {
                System.out.println(e.getCause());
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }

    }

    /**
     * Function to change from the home scene over to the edit scene with information about a task
     * It will load in with the same stylesheet as the home scene.
     *
     * @param event the event
     * @param task  the task that the initial information will be read from
     * @throws IOException the io exception
     */
    public void changeScenes(ActionEvent event, Task task) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Edit.fxml"));
        Parent parent = loader.load();

        Scene main = new Scene(parent);

        // Access the controller for the destination
        EditController editController = loader.getController();
        editController.initData(tableView.getSelectionModel().getSelectedItem());

        //Get the Stage objecty from the button event
        Stage primaryStage = (Stage) ((Node)event.getSource()).getScene().getWindow();

        main.getStylesheets().add(Main.getStylesheet());
        primaryStage.setTitle("Edit Task");
        primaryStage.setScene(main);
        primaryStage.show();
    }

    /**
     * Archive task button clicked. when the button is pushed it will change the home scene over to the archive scene.
     * It will load in with the same stylesheet as the home scene.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void archiveTaskButtonClicked(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("Archive.fxml"));
        Scene archiveScene = new Scene(root);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        archiveScene.getStylesheets().add(Main.getStylesheet());
        window.setScene(archiveScene);
    }

    /**
     * Method to change scene over to settings scene
     * It will load in with the same stylesheet as the home scene.
     *
     * @param event event to get the already running scene of the program
     * @throws IOException Exception that happens when a file is not loaded correctly
     */
    public void settingsButtonClicked(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("Settings.fxml"));
        Scene settingsScene = new Scene(root);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        settingsScene.getStylesheets().add(Main.getStylesheet());
        window.setScene(settingsScene);
    }

    /**
     * Method to get which table that is going to be visible on launch
     *
     * @throws IOException the io exception
     */
    public void visible() throws IOException {
        File settings = new File("settings.txt");
        BufferedReader br = new BufferedReader(new FileReader("settings.txt"));
        boolean exists = settings.exists();

        if (exists) {
            Main.readWrite.readSettings();
        }
        else {
            for (int i = 0; i <= 7; i++) {
                visibleColumns.add(true);
            }
            Main.readWrite.writeSettings();
        }

        if (br.readLine() == null) {
            for (int i = 0; i <= 7; i++) {
                visibleColumns.add(true);
            }
            Main.readWrite.writeSettings();
        }

       priorityColumn.setVisible(visibleColumns.get(0));
       statusColumn.setVisible(visibleColumns.get(1));
       categoryColumn.setVisible(visibleColumns.get(2));
       startDateColumn.setVisible(visibleColumns.get(3));
       deadlineDateColumn.setVisible(visibleColumns.get(4));
       deadlineClockColumn.setVisible(visibleColumns.get(5));
       taskDescriptionColumn.setVisible(visibleColumns.get(6));
       finishedBoolColumn.setVisible(visibleColumns.get(7));
    }
}
