package no.ntnu.IDATG1002.v2021.group5.GUI;

/**
 * Dummy class so that the .jar file can run
 */
public class SuperMain {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Main.main(args);
    }
}
