package no.ntnu.IDATG1002.v2021.group5.GUI;

import no.ntnu.IDATG1002.v2021.group5.Tasks.Task;
import no.ntnu.IDATG1002.v2021.group5.Tasks.TaskRegister;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import javafx.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.Time;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The type Archive controller. Controls everything
 * that has to do with the archive window.
 */
public class ArchiveController implements Initializable {

    @FXML private TableView<Task> archiveTableView;
    @FXML private TableColumn<Task, String> taskNameColumn;
    @FXML private TableColumn<Task, String> priorityColumn;
    @FXML private TableColumn<Task, String> categoryColumn;
    @FXML private TableColumn<Task, LocalDate> startDateColumn;
    @FXML private TableColumn<Task, LocalDate> endDateColumn;
    @FXML private TableColumn<Task, Time> deadlineTimeColumn;
    @FXML private TableColumn<Task, String> taskDescriptionColumn;
    @FXML private TableColumn<Task, Boolean> finishedBoolColumn;

    @FXML private Button deleteButton;
    @FXML private Button settingsButton;
    @FXML private Button editButton;

    /**
     * The constant archiveRegister.
     */
    public static TaskRegister archiveRegister = new TaskRegister();

    /**
     * The Archive data.
     */
    public ObservableList<Task> archiveData;
    
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        archiveTableView.setEditable(true);

        taskNameColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("taskName"));
        priorityColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("priority"));
        categoryColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("category"));
        startDateColumn.setCellValueFactory(new PropertyValueFactory<Task, LocalDate>("startDate"));
        endDateColumn.setCellValueFactory(new PropertyValueFactory<Task, LocalDate>("endDate"));
        deadlineTimeColumn.setCellValueFactory(new PropertyValueFactory<Task, Time>("deadlineClock"));
        taskDescriptionColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("taskDescription"));
        finishedBoolColumn.setCellValueFactory(param -> param.getValue().obsBoolean());
        /*
         * code is from:: https://stackoverflow.com/questions/39270033/javafx-checkboxtablecell-catch-clickevent
         */
        finishedBoolColumn.setCellFactory(p -> {
            CheckBox checkBox = new CheckBox();
            TableCell<Task, Boolean> tableCell = new TableCell<>(){
                @Override
                protected void updateItem(Boolean item, boolean empty) {

                    super.updateItem(item, empty);
                    if (empty || item == null)
                        setGraphic(null);
                    else {
                        setGraphic(checkBox);
                        checkBox.setSelected(item);
                    }
                }
            };

            checkBox.addEventFilter(MouseEvent.MOUSE_PRESSED, event ->
            {
                try {
                    changeFinished(checkBox, (Task) tableCell.getTableRow().getItem());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            checkBox.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
                if(event.getCode() == KeyCode.SPACE) {
                    try {
                        changeFinished(checkBox, (Task) tableCell.getTableRow().getItem());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            tableCell.setAlignment(Pos.CENTER);
            tableCell.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

            return tableCell;
        });

        // Comparing priorities by their index rather than alphabetically
        priorityColumn.setComparator(Comparator.comparingInt(p -> Task.getPriorityNames().indexOf(p)));

        //Method to have the same view as the home tableView
        //Du kan fjerne denne funksjonen om du mener det ikke er noen grunn for å ha det
        archiveVisible();

        if (!archiveRegister.getTasks().isEmpty())  {
            archiveRegister.clearReg();
        }

        // Sorting the tasks from storage and only keeping the finished tasks.
        for(Task task : Main.taskRegister.getTasks()) {
            if(task.isFinished()) {
                archiveRegister.addTask(task);
            }
        }
        archiveData = archiveRegister.getTasks();

        archiveTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        archiveTableView.getItems().clear();

        archiveTableView.setItems(archiveData);

        Main.setPreviousWindow("Archive");

        setTooltipsForButtons();
    }

    /**
     * Function to make a task finished without going through edit window.
     * Done by "unselecting" a checkbox.
     *
     * @param checkBox selects the checkBox to the row
     * @param item the task the checkBox is a part of
     * @throws IOException
     */
    private void changeFinished(CheckBox checkBox, Task item) throws IOException {
        // Validate here
        checkBox.setSelected(!checkBox.isSelected());
        Task tempTask = item;
        archiveData.remove(item);
        Main.readWrite.removeTaskFromFile(item);
        tempTask.setFinished(new SimpleBooleanProperty(false));
        Main.readWrite.writeToFile(tempTask);
    }

    /**
     * Adding tooltips for all the buttons.
     */
    private void setTooltipsForButtons() {
        // Set hover text for buttons
        deleteButton.setTooltip(new Tooltip("Delete task"));
        settingsButton.setTooltip(new Tooltip("Settings"));
    }

    /**
     * Back button clicked. The scene is changed back to
     * the home view.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void backButtonClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Home.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene.getStylesheets().add(Main.getStylesheet());

        stage.setScene(scene);
    }

    /**
     * When settings button is clicked the scene
     * will change to the settings scene.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void settingsButtonClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Settings.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene.getStylesheets().add(Main.getStylesheet());

        stage.setScene(scene);
    }

    /**
     * Displays a delete confirmation dialog. If the user confirms the delete,
     * <code>true</code> is returned.
     * <p>
     * KODE FRA ARNE STYVE: JavaFX-TableView-with-Dialog
     *
     * @return <code>true</code> if the user confirms the delete
     */
    public boolean showDeleteConfirmationDialog() {
        boolean deleteConfirmed = false;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete confirmation");
        alert.setHeaderText("Are you sure you want to delete this item?");

        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(Main.getStylesheet());

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent()) {
            deleteConfirmed = (result.get() == ButtonType.OK);
        }
        return deleteConfirmed;
    }

    /**
     * When the delete button is clicked the selected task will
     * be removed from the table and from the file.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void deleteButtonClicked(ActionEvent event) throws IOException {
        if (!archiveTableView.getSelectionModel().isEmpty())  {
            if (showDeleteConfirmationDialog())  {
                Task tempTask = archiveTableView.getSelectionModel().getSelectedItem();
                archiveData.remove(tempTask);
                Main.readWrite.removeTaskFromFile(tempTask);
            }
        }
    }

    /**
     * When edit task button is clicked the scene will change
     * to the edit window, if a task is selected.
     *
     * @param event the event
     */
    public void editTaskButtonClicked(ActionEvent event){
        if (!archiveTableView.getSelectionModel().isEmpty())
        {
            try  {
                Task task = archiveTableView.getSelectionModel().getSelectedItem();
                changeScenes(event, task);
            }
            catch (IOException e)  {
                e.printStackTrace();
            }
        }

    }

    /**
     * Method for changing scenes. Used for the edit functionality.
     *
     * @param event the event
     * @param task  the task
     * @throws IOException the io exception
     */
    public void changeScenes(ActionEvent event, Task task) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Edit.fxml"));
        //Parent root = FXMLLoader.load(getClass().getResource("Edit.fxml"));
        Parent parent = loader.load();

        Scene main = new Scene(parent);

        // Access the controller for the destination
        EditController editController = loader.getController();
        editController.initData(archiveTableView.getSelectionModel().getSelectedItem());

        //Get the Stage objecty from the button event
        Stage primaryStage = (Stage) ((Node)event.getSource()).getScene().getWindow();

        main.getStylesheets().add(Main.getStylesheet());
        primaryStage.setTitle("Edit Task");
        primaryStage.setScene(main);
        primaryStage.show();
    }



    /**
     * Method to control which columns are visible in the tableView.
     * The same columns as in the home window
     */
    private void archiveVisible(){
        priorityColumn.setVisible(HomeController.visibleColumns.get(0));
        categoryColumn.setVisible(HomeController.visibleColumns.get(2));
        startDateColumn.setVisible(HomeController.visibleColumns.get(3));
        endDateColumn.setVisible(HomeController.visibleColumns.get(4));
        deadlineTimeColumn.setVisible(HomeController.visibleColumns.get(5));
        taskDescriptionColumn.setVisible(HomeController.visibleColumns.get(6));
    }
}
