package no.ntnu.IDATG1002.v2021.group5.Tasks;

import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableBooleanValue;

import java.time.LocalDate;
import java.util.List;

/**
 * The type Task.
 */
public class Task {


    private String taskName;
    private String category;
    private String priority;
    private String status;
    private LocalDate startDate;
    private LocalDate endDate;
    private String deadlineClock;
    private String taskDescription;
    private BooleanProperty finished;

    /**
     * Instantiates a new Task.
     *
     * @param taskName        the task name
     * @param priority        the priority
     * @param status          the status
     * @param category        the category
     * @param startDate       the start date
     * @param endDate         the end date
     * @param deadlineClock   the deadline clock
     * @param taskDescription the task description
     * @param finished        the finished
     */
    public Task(
            @JsonProperty("taskName")           String taskName,
            @JsonProperty("priority")           String priority,
            @JsonProperty("status")             String status,
            @JsonProperty("category")           String category,
            @JsonProperty("startDate")          LocalDate startDate,
            @JsonProperty("endDate")            LocalDate endDate,
            @JsonProperty("deadlineClock")      String deadlineClock,
            @JsonProperty("taskDescription")    String taskDescription,
            @JsonProperty("finished")           String finished) {
        this.taskName = taskName;
        this.category = category;
        this.priority = priority;
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
        this.deadlineClock = deadlineClock;
        this.taskDescription = taskDescription;
        if (finished.equals("true"))  {
            this.finished = new SimpleBooleanProperty(true);
        }
        else  {
            this.finished = new SimpleBooleanProperty(false);
        }
    }

    /**
     * Gets task name.
     *
     * @return the task name
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Sets task name.
     *
     * @param taskName the task name
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets category.
     *
     * @param category the category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Gets task description.
     *
     * @return the task description
     */
    public String getTaskDescription() {
        return taskDescription;
    }

    /**
     * Sets task description.
     *
     * @param taskDescription the task description
     */
    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    /**
     * Is finished boolean.
     *
     * @return the boolean
     */
    public boolean isFinished() {
        return finished.getValue();
    }

    /**
     * Obs boolean observable boolean value.
     *
     * @return the observable boolean value
     */
    public ObservableBooleanValue obsBoolean(){
        return finished;
    }

    /**
     * Check box finished boolean property.
     *
     * @return the boolean property
     */
    public BooleanProperty checkBoxFinished(){
        return finished;
    }

    /**
     * Sets finished.
     *
     * @param finished the finished
     */
    public void setFinished(SimpleBooleanProperty finished) {
        this.finished = finished;
    }

    /**
     * Gets priority.
     *
     * @return the priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets priority.
     *
     * @param priority the priority
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets start date.
     *
     * @return the start date
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * Sets start date.
     *
     * @param startDate the start date
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets end date.
     *
     * @return the end date
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * Sets end date.
     *
     * @param endDate the end date
     */
    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * Gets deadline clock.
     *
     * @return the deadline clock
     */
    public String getDeadlineClock() {
        return deadlineClock;
    }

    /**
     * Sets deadline clock.
     *
     * @param deadlineClock the deadline clock
     */
    public void setDeadlineClock(String deadlineClock) {
        this.deadlineClock = deadlineClock;
    }

    /**
     * Get priority names as a list
     *
     * @return list of priority names
     */
    public static List<String> getPriorityNames(){
        //The list is used for sorting in table.
        return List.of("High", "Medium", "Low");
    }

    @Override
    public String toString() {
        return taskName+';'+priority+';'+status+';'+category+';'+startDate.toString()+';'+endDate.toString()+';'+
                deadlineClock+';'+taskDescription+';'+finished;

    }
}
