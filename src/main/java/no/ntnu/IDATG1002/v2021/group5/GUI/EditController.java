package no.ntnu.IDATG1002.v2021.group5.GUI;

import no.ntnu.IDATG1002.v2021.group5.Tasks.Task;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type Edit controller.
 */
public class EditController implements Initializable {

    private Task selectedTask;

    @FXML private TextField taskNameField;
    @FXML private ChoiceBox priorityBox;
    @FXML private ChoiceBox statusBox;
    @FXML private ChoiceBox catBox;
    @FXML private DatePicker startDateField;
    @FXML private DatePicker endDateField;
    @FXML private TextField deadlineField;
    @FXML private TextArea taskDescriptionField;
    @FXML private CheckBox taskCompletedField;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        priorityBox.getItems().addAll("Low", "Medium", "High");
        statusBox.getItems().addAll("Not Started", "Ongoing");
        catBox.getItems().addAll("Work", "School", "Sport", "Hobby", "Home", "Other");
    }


    /**
     * Init data.
     *
     * @param task the task
     */
    public void initData (Task task)  {
        this.selectedTask = task;

        this.taskNameField.setText(selectedTask.getTaskName());
        this.priorityBox.setValue(selectedTask.getPriority());
        this.statusBox.setValue(selectedTask.getStatus());
        this.catBox.setValue(selectedTask.getCategory());
        this.startDateField.setValue(selectedTask.getStartDate());
        this.endDateField.setValue(selectedTask.getEndDate());
        this.deadlineField.setText(selectedTask.getDeadlineClock());
        this.taskDescriptionField.setText(selectedTask.getTaskDescription());
        if (selectedTask.isFinished())  {
            this.taskCompletedField.selectedProperty().setValue(true);
        }
    }

    /**
     * Return home.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void returnHome(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();

        if (Main.getPreviousWindow().equalsIgnoreCase("Archive"))  {
            loader.setLocation(getClass().getResource("Archive.fxml"));
        }
        else {
            loader.setLocation(getClass().getResource("Home.fxml"));
        }
        

        Parent root = loader.load();

        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene.getStylesheets().add(Main.getStylesheet());

        stage.setScene(scene);
    }

    /**
     * Confirm clicked.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void confirmClicked(ActionEvent event) throws IOException {
        if(taskNameField.getText().isEmpty()){
            taskNameField.setText("A new task");
        }
        if(deadlineField.getText().isEmpty()){
            deadlineField.setText("12:00");
        }
        if(taskDescriptionField.getText().isEmpty()){
            taskDescriptionField.setText("This task needs a description.");
        }

        Task newTask = new Task(taskNameField.getText(), priorityBox.getValue().toString(), statusBox.getValue().toString(),
                catBox.getValue().toString(), startDateField.getValue(), endDateField.getValue(), deadlineField.getText(),taskDescriptionField.getText(), "false");

        if (taskCompletedField.isSelected())  {
            newTask.setFinished(new SimpleBooleanProperty(true));
        }

        Main.readWrite.removeTaskFromFile(selectedTask);
        Main.readWrite.writeToFile(newTask);
        Main.readWrite.readFile();

        returnHome(event);
    }

    /**
     * Task completed clicked.
     *
     * @throws IOException the io exception
     */
    public void taskCompletedClicked() throws IOException  {
        if (taskCompletedField.selectedProperty().getValue())  {
            statusBox.setValue("Finished");
        }
    }


    /**
     * Cancel button.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    public void cancelButton(ActionEvent event) throws IOException {
        returnHome(event);
    }

}
