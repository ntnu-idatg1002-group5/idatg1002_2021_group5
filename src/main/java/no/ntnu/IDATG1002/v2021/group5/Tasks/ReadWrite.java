package no.ntnu.IDATG1002.v2021.group5.Tasks;

import no.ntnu.IDATG1002.v2021.group5.GUI.HomeController;
import no.ntnu.IDATG1002.v2021.group5.GUI.Main;

import java.io.*;
import java.nio.file.Files;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;

/**
 * The type Read write.
 */
public class ReadWrite {
    private static final String tasksFileName = "tasks.json";
    /**
     * The File.
     */
    static final File file = new File(tasksFileName);

    /**
     * Instantiates a new Read write.
     *
     * @throws IOException the io exception
     */
    public ReadWrite() throws IOException {
        readFile();
    }

    /**
     * Write to file.
     *
     * @param task the task
     */
    public void writeToFile(Task task){
        // Creating Object of ObjectMapper define in Jackson API
        ObjectMapper mapper = JsonMapper.builder()
                .findAndAddModules()
                .build();
        try {
            // Converting the Java object into a JSON string
            String jsonStr = mapper.writeValueAsString(task);
            // Displaying Java object into a JSON string
            FileWriter writer = new FileWriter(tasksFileName, true);
            BufferedWriter out = new BufferedWriter(writer);
            out.write(jsonStr);
            out.newLine();
            out.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read file.
     * This method used JSON mapping in order to read
     * from a JSON file.
     */
    public void readFile(){
        Main.taskRegister.clearReg();
        ObjectMapper mapper = JsonMapper.builder()
                .findAndAddModules()
                .build();
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                try {
                    Task newTask = mapper.readValue(data, Task.class);
                    Main.taskRegister.addTask(newTask);
                } catch (JsonProcessingException e) {
                        System.out.println("An json parsing error occurred.");
                        e.printStackTrace();
                    }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /**
     * Creates the files necessary for the application to run.
     * This method also uses try/catch to make sure that
     * you get a message incase the creation of the file fails.
     */
    public static void createFiles() {
        try {
            file.createNewFile();
            (new File("settings.txt")).createNewFile();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /**
     * Remove task from file.
     * This method also uses JSON in order to delete a task from
     * the JSON file.
     *
     * @param task the task
     * @throws IOException the io exception
     */
    public void removeTaskFromFile(Task task) throws IOException {
        ObjectMapper mapper = JsonMapper.builder()
                .findAndAddModules()
                .build();
        String jsonStr = mapper.writeValueAsString(task);

        File temp = new File("_temp_.txt");
        PrintWriter out = new PrintWriter(new FileWriter(temp));

        Files.lines(file.toPath())
                .filter(line -> !line.equals(jsonStr))
                .forEach(out::println);

        out.close();
        file.delete();
        temp.renameTo(file);

        Main.taskRegister.removeTask(task);
    }

    /**
     * Write settings.
     */
    public void writeSettings() {
        try {
            File settingsFile = new File("settings.txt");
            File temp = new File("_tempSettings_.txt");
            BufferedWriter out = new BufferedWriter(new FileWriter(temp));
            int write = 0;
            while (write < HomeController.visibleColumns.size()){
                out.write(HomeController.visibleColumns.get(write).toString() + ";");
                out.newLine();
                write++;
            }
            //Which stylesheet that was last in use when program was running
            out.write(Main.getStylesheet());
            out.close();
            settingsFile.delete();
            temp.renameTo(settingsFile);
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Read settings.
     */
    public void readSettings(){
        try {
            Scanner myReader = new Scanner(new File("settings.txt"));
            int read = 0;
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] temp = data.split(";");
                if (HomeController.visibleColumns.size() == 8){
                    //Same stylesheet as when program was last running
                    Main.setStylesheet(data);
                }
                else {
                    HomeController.visibleColumns.add(read, Boolean.parseBoolean(temp[0]));
                }
                read++;
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
