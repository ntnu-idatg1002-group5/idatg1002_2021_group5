module PlandaList {
    requires javafx.base;
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.archetype.fxml;
    requires java.sql;

    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.datatype.jsr310;
    opens no.ntnu.IDATG1002.v2021.group5.GUI to javafx.fxml;
    opens no.ntnu.IDATG1002.v2021.group5.Tasks;
//    opens no.ntnu.IDATG1002.v2021.group5.GUI;

    exports no.ntnu.IDATG1002.v2021.group5.GUI;
    exports no.ntnu.IDATG1002.v2021.group5.Tasks;
}