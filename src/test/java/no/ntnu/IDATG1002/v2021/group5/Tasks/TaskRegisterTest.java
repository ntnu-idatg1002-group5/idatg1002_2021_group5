package no.ntnu.IDATG1002.v2021.group5.Tasks;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class TaskRegisterTest {



    @DisplayName("Positive test for Task Register constructor")
    @Test
    public void taskRegisterConstructorTestPositive()
    {
        TaskRegister taskRegister = new TaskRegister();
        assertEquals(0, taskRegister.tasks.size());
    }

    @DisplayName("Positive test for add task")
    @Test
    public void addTaskTestPositive()  {
        TaskRegister taskRegister = new TaskRegister();
        taskRegister.addTask(new Task("taskName", "High", "Ongoing","taskCategory",
                LocalDate.of(2000, 1, 1), LocalDate.of(2020, 1, 1),
                "12:00", "taskDescription", "true"));
        assertEquals(1, taskRegister.tasks.size());
    }

    @DisplayName("Negative test for add task, null task trying to be added")
    @Test
    public void addTaskTestNegative()  {
        TaskRegister taskRegister = new TaskRegister();
        try {
            taskRegister.addTask(null);
            fail("Test should throw Illegal Argument Exception");
        }
        catch (IllegalArgumentException e)  {
            assert(true);
        }
    }


    @DisplayName("Positive test for remove task")
    @Test
    public void removeTaskTestPositive()  {
        TaskRegister taskRegister = new TaskRegister();
        Task task = new Task("taskName", "High", "Ongoing","taskCategory",
                LocalDate.of(2000, 1, 1), LocalDate.of(2020, 1, 1),
                "12:00", "taskDescription", "true");
        taskRegister.addTask(task);
        taskRegister.removeTask(task);
        assertEquals(0, taskRegister.tasks.size());
    }

    @DisplayName("Negative test for remove task, task not found in register")
    @Test
    public void removeTaskTestNegativeEmptyRegister()  {
        TaskRegister taskRegister = new TaskRegister();
        Task task = new Task("taskName", "High", "Ongoing","taskCategory",
                LocalDate.of(2000, 1, 1), LocalDate.of(2020, 1, 1),
                "12:00", "taskDescription", "true");
        taskRegister.removeTask(task);
        assert(true);
    }


    @DisplayName("Positive test for clear register")
    @Test
    public void clearRegTestPositive()  {
        TaskRegister taskRegister = new TaskRegister();
        taskRegister.addTask(new Task("taskName", "High", "Ongoing","taskCategory",
                LocalDate.of(2000, 1, 1), LocalDate.of(2020, 1, 1),
                "12:00", "taskDescription", "true"));
        taskRegister.addTask(new Task("taskName2", "High", "Ongoing","taskCategory",
                LocalDate.of(2000, 1, 1), LocalDate.of(2020, 1, 1),
                "12:00", "taskDescription", "true"));
        taskRegister.addTask(new Task("taskName3", "High", "Ongoing","taskCategory",
                LocalDate.of(2000, 1, 1), LocalDate.of(2020, 1, 1),
                "12:00", "taskDescription", "true"));
        taskRegister.clearReg();
        assertEquals(0, taskRegister.tasks.size());
    }






}