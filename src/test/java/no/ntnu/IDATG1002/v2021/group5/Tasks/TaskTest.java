package no.ntnu.IDATG1002.v2021.group5.Tasks;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {

    private static Task task;

    @BeforeAll
    public static void setUp()  {
        task = new Task("taskName", "High", "Ongoing","taskCategory",
                LocalDate.of(2000, 1, 1), LocalDate.of(2020, 1, 1),
                "12:00", "taskDescription", "false");
    }

    @DisplayName("Positive test for Task constructor plus get methods")
    @Test
    public void taskConstructorTestPositive()  {
        Task constructorTask = new Task("taskName", "High", "Ongoing","taskCategory",
                LocalDate.of(2000, 1, 1), LocalDate.of(2020, 1, 1),
                "12:00", "taskDescription", "true");
        assertEquals("taskName", constructorTask.getTaskName());
        assertEquals("High",constructorTask.getPriority());
        assertEquals("Ongoing", constructorTask.getStatus());
        assertEquals("taskCategory", constructorTask.getCategory());
        assertEquals(LocalDate.of(2000, 1, 1), constructorTask.getStartDate());
        assertEquals(LocalDate.of(2020, 1, 1), constructorTask.getEndDate());
        assertEquals("12:00", constructorTask.getDeadlineClock());
        assertEquals("taskDescription", constructorTask.getTaskDescription());
        assertEquals(true, constructorTask.isFinished());
    }

    @DisplayName("Positive test for set task name")
    @Test
    public void setTaskNameTestPositive()  {
        task.setTaskName("NewTaskName");
        assertEquals("NewTaskName", task.getTaskName());
    }

    @DisplayName("Positive test for set category")
    @Test
    public void setCategoryNameTestPositive()  {
        task.setCategory("NewCategory");
        assertEquals("NewCategory", task.getCategory());
    }

    @DisplayName("Positive test for set task description")
    @Test
    public void setTaskDescriptionTestPositive()  {
        task.setTaskDescription("NewDescription");
        assertEquals("NewDescription", task.getTaskDescription());
    }

    @DisplayName("Positive test for set priority")
    @Test
    public void setPriorityTestPositive()  {
        task.setPriority("High");
        assertEquals("High", task.getPriority());
    }

    @DisplayName("Positive test for set status")
    @Test
    public void setStatusTestPositive()  {
        task.setStatus("Ongoing");
        assertEquals("Ongoing", task.getStatus());
    }

    @DisplayName("Positive test for set start date")
    @Test
    public void setStartDateTestPositive()  {
        task.setStartDate(LocalDate.of(2021, 2, 2));
        assertEquals(LocalDate.of(2021, 2, 2), task.getStartDate());
    }

    @DisplayName("Positive test for set end date")
    @Test
    public void setEndDateTestPositive()  {
        task.setEndDate(LocalDate.of(2021, 2, 2));
        assertEquals(LocalDate.of(2021, 2, 2), task.getEndDate());
    }

    @DisplayName("Positive test for set deadline time")
    @Test
    public void setDeadlineTestPositive()  {
        task.setDeadlineClock("23:59");
        assertEquals("23:59", task.getDeadlineClock());
    }









}