# Planda
Planda is a Java to-do-list application.  

## Install and Run
1. Download the latest version from the [releases](https://gitlab.stud.idi.ntnu.no/ntnu-idatg1002-group5/idatg1002_2021_group5/-/releases) page.
2. Unzip and open the folder you just downloaded.
3. Make sure that Java is installed. 
   If not, you can download it from here: 
   [Java](https://adoptopenjdk.net/?variant=openjdk16&jvmVariant=hotspot) (JRE14+ is required)
4. Start Planda.jar (alternatively use the 
   PlandaRunHelper.bat if the program won't start)
   
## Usage
Please reference the [User Manual](https://gitlab.stud.idi.ntnu.no/ntnu-idatg1002-group5/idatg1002_2021_group5/-/wikis/User%20Manual)

## License notice
This is the official notice outlined in the license of
software we use. A detailed license is provided in [licenses.md](licenses.md)
* [Bootstrap Icons License (MIT)](https://github.com/twbs/icons/blob/main/LICENSE.md)
